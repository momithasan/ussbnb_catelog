-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.11-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table uss_bnb_catalog.buildings
CREATE TABLE IF NOT EXISTS `buildings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- Dumping data for table uss_bnb_catalog.buildings: ~8 rows (approximately)
/*!40000 ALTER TABLE `buildings` DISABLE KEYS */;
INSERT INTO `buildings` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Hobujaama', NULL, NULL, NULL),
	(2, 'Vabaduse', NULL, NULL, NULL),
	(3, 'Gedimino Avenue', NULL, NULL, NULL),
	(4, 'Valdemara', NULL, NULL, NULL),
	(6, 'Didzioji', NULL, NULL, NULL),
	(7, 'Maakri', NULL, NULL, NULL),
	(8, 'Telegraph', NULL, NULL, NULL),
	(9, 'M Test', '2020-03-12 02:29:09', '2020-03-12 02:39:30', NULL),
	(10, 'Momit Plaza RN', '2020-03-16 10:51:36', '2020-03-16 10:51:36', NULL);
/*!40000 ALTER TABLE `buildings` ENABLE KEYS */;

-- Dumping structure for table uss_bnb_catalog.rooms
CREATE TABLE IF NOT EXISTS `rooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `building_id` int(11) NOT NULL,
  `room_name` varchar(32) NOT NULL,
  `details` varchar(255) DEFAULT NULL,
  `price_per_min` double(12,2) DEFAULT NULL,
  `maximum_person` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;

-- Dumping data for table uss_bnb_catalog.rooms: ~20 rows (approximately)
/*!40000 ALTER TABLE `rooms` DISABLE KEYS */;
INSERT INTO `rooms` (`id`, `building_id`, `room_name`, `details`, `price_per_min`, `maximum_person`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(16, 2, 'Brainstorm', '', 0.00, 0, NULL, NULL, NULL),
	(17, 3, 'Jogailos', '', 0.00, 0, NULL, NULL, NULL),
	(18, 3, 'Gedimino', '', 0.00, 0, NULL, NULL, NULL),
	(19, 3, 'St.Georg hall', '', 0.00, 0, NULL, NULL, NULL),
	(20, 4, 'Daugava', '', 0.00, 0, NULL, NULL, NULL),
	(21, 4, 'Lielupe', '', 0.00, 0, NULL, NULL, NULL),
	(22, 4, 'Abava', '', 0.00, 0, NULL, NULL, NULL),
	(27, 4, 'Lounge', '', 0.00, 0, NULL, NULL, NULL),
	(28, 1, 'Big Ideas', '', 0.00, 0, NULL, NULL, NULL),
	(29, 1, 'Synergy', '', 0.00, 0, NULL, NULL, NULL),
	(30, 2, 'Inspire', '', 0.00, 0, NULL, NULL, NULL),
	(31, 2, 'Create', '', 0.00, 0, NULL, NULL, NULL),
	(32, 2, 'Innovate', '', 0.00, 0, NULL, NULL, NULL),
	(33, 2, 'Conquer', '', 0.00, 0, NULL, NULL, NULL),
	(34, 2, 'Event area', '', 0.00, 0, NULL, NULL, NULL),
	(35, 7, 'Think Wild', '', 0.00, 0, NULL, NULL, NULL),
	(36, 7, 'Dream Wild', '', 0.00, 0, NULL, NULL, NULL),
	(37, 7, 'Imagine Wild', '', 0.00, 0, NULL, NULL, NULL),
	(38, 7, 'Podcast & Audio Recording Room', '', 0.00, 0, NULL, NULL, NULL),
	(39, 8, 'Morse Code', '', 0.00, 0, NULL, NULL, NULL),
	(40, 9, 'Momit\'s Room TT', 'Some details', 12.00, 2, '2020-03-12 07:14:30', '2020-03-16 11:20:03', NULL);
/*!40000 ALTER TABLE `rooms` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
