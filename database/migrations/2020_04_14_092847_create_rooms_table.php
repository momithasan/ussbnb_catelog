<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('building_id');
            $table->string('name');
            $table->string('details')->nullable();
            $table->float('price_per_hour');
            $table->integer('maximum_person');
            $table->tinyInteger('room_type');
            $table->time('working_hours_from')->nullable();
            $table->time('working_hours_till')->nullable();
            $table->tinyInteger('status');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('building_id')->references('id')->on('buildings');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
