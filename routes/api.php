<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::group(['prefix' => '/v1','middleware' => ['secure']], function() {
Route::group(['prefix' => '/v1'], function() {
	//Buildings
	Route::get('/buildings', array('as'=>'Building List', 'uses'=>'BuildingController@buildings'));
	Route::get('/buildings/{id}', array('as'=>'Building Details', 'uses'=>'BuildingController@getBuildingDetail'));
    Route::get('/building/{id}/edit', array('as'=>'Building By Id', 'uses'=>'BuildingController@getBuildingbyId'));
	Route::put('/buildings/{id}', array('as'=>'Building Update', 'uses'=>'BuildingController@updateBuilding'));
	Route::post('/buildings', array('as'=>'Create Building', 'uses'=>'BuildingController@createBuilding'));
	Route::delete('/buildings/{id}', array('as'=>'Delete Building', 'uses'=>'BuildingController@deleteBuilding'));

	//Rooms
	Route::get('/rooms', array('as'=>'Building List', 'uses'=>'RoomController@rooms'));
	Route::get('/rooms/{id}', array('as'=>'Room Details', 'uses'=>'RoomController@getRoomDetail'));
	Route::get('/room/{id}/edit', array('as'=>'Room Edit', 'uses'=>'RoomController@getEditRoomDetail'));
	Route::put('/rooms/{id}', array('as'=>'Room Update', 'uses'=>'RoomController@updateRoom'));
	Route::post('/rooms', array('as'=>'Create Room', 'uses'=>'RoomController@createRoom'));
	Route::delete('/rooms/{id}', array('as'=>'Delete Room', 'uses'=>'RoomController@deleteRoom'));
    
    // Country
    Route::post('/countries', array('as'=>'Create Country', 'uses'=>'CountryController@createCountry'));
    Route::get('/countries', array('as'=>'List Countries', 'uses'=>'CountryController@getCountryList'));
    Route::get('/countries/{id}', array('as'=>'Country Details', 'uses'=>'CountryController@getCountryById'));
    Route::put('/countries/{id}', array('as'=>'Country Update', 'uses'=>'CountryController@updateCountry'));
    Route::delete('/countries/{id}', array('as'=>'Country Delete', 'uses'=>'CountryController@deleteCountry'));
    Route::get('/search/country', array('as'=>'Country search', 'uses'=>'CountryController@SearchCountryByName'));

    // Locations
    Route::post('/locations', array('as'=>'Create Location', 'uses'=>'LocationController@createLocation'));
    Route::get('/locations', array('as'=>'List Location', 'uses'=>'LocationController@getAllLocations'));
    Route::get('/locations/{id}', array('as'=>'Location Detail', 'uses'=>'LocationController@getLocationById'));

    Route::put('/locations/{id}', array('as'=>'Location Update', 'uses'=>'LocationController@updateLocation'));
    Route::delete('/locations/{id}', array('as'=>'Location delete', 'uses'=>'LocationController@deleteLocation'));

    Route::post('/search/location', array('as'=>'Location search', 'uses'=>'LocationController@SearchLocationByName'));
    Route::get('/location/detail/{id}', array('as'=>'Location Detail', 'uses'=>'LocationController@getLocationDetails'));

});
