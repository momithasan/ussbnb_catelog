<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    //
    protected $table = 'locations';
    protected $fillable = ['location_name', 'country_id', 'status'];

    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    public function building() {
        return $this->belongsTo('App\Building');
    }
}
