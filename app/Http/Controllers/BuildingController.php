<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Traits\ApiResponser;
use Illuminate\Support\Str;
use Validator;
use App\Building;
use App\Room;
use App\Location;
use App\Image;
use Exception;
use Illuminate\Support\Facades\Log;

class BuildingController extends Controller
{
    use ApiResponser;

	public function __construct()
    {

    }
	// get all buildings
	function buildings(){
	    try {
            $buildings = Building::with('image','locations','locations.country')
                        ->get();
            $totalBuildings = count($buildings);

            if($buildings) {

                $data = [
                    'success' => true,
                    'message' => 'Building list',
                    'code' => Response::HTTP_OK,
                    'locale'=>'',
                    'data' => [
                        'buildings'=> $buildings
                    ]
                ];

                return $this->successResponse( $data, Response::HTTP_ACCEPTED);
            }

            return $this->errorResponse('failed to get all the records', Response::HTTP_NOT_FOUND);

        } catch(Exception $e) {
	        return $this->errorResponse($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
	}

	//get a single building details
	function getBuildingDetail($id){
	    try {
            $building = Building::where('id', $id)->with('image','locations','locations.country')->first();
                $data = [
                    'success' => true,
                    'message' => 'Single building',
                    'code' => Response::HTTP_OK,
                    'locale'=>'',
                    'data' => [
                        'buildings'=> $building
                    ]
                ];
            Log::debug("buildings", array(0=>$building));
            if ($building) {
                return $this->successResponse(json_encode($data), Response::HTTP_OK);
            }

            return $this->errorResponse('No building record found', Response::HTTP_NOT_FOUND);
        } catch(Exception $e) {
	        return $this->errorResponse($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }

	}

    //get a single building details
    function getBuildingById($id){
        try {

            $building = Building::where('id',$id)->with('image')->first();

            $data = [
                'success' => true,
                'message' => 'Single building',
                'code' => Response::HTTP_OK,
                'locale'=>'',
                'data' => [
                    'buildings'=> $building
                ]
            ];
            if ($building) {
                return $this->successResponse($data, Response::HTTP_OK);
            }

            return $this->errorResponse('No buildings record found', Response::HTTP_NOT_FOUND);
        } catch(Exception $e) {
            return $this->errorResponse("Unable to get building details", Response::HTTP_BAD_REQUEST);
        }

    }

	//insert new buildings
	function createBuilding(Request $request){
		//var_dump($request->all());die;
        $rule = [
            'name' => 'required|string',
            'location_id' => 'required',
            'address' => 'required|string',
            'status' => 'required',
        ];
        $validation = Validator::make($request->all(), $rule);

        if($validation->fails()){
            $return_arr = array('status' => 'error', 'msg' => 'Please fill all required field.');
            return $this->errorResponse('Validation Fails', Response::HTTP_NOT_ACCEPTABLE);
        }

        try {

            $building = Building::create([
                'name' =>  $request->input('name'),
                'location_id' =>  $request->input('location_id'),
                'address' => $request->input('address'),
                'status' => 1,
            ]);

            if($building) {
                $this->_storeImage($building, $request);

                $data = [
                    'success' => true,
                    'message' => 'Building created',
                    'code' => Response::HTTP_CREATED,
                    'locale'=>'',
                    'data' => [
                        'buildings'=> $building
                    ]
                ];

                return $this->successResponse($data, Response::HTTP_CREATED);
            }
            return $this->errorResponse("Error, Fail to create a building ", Response::HTTP_FORBIDDEN);

        } catch(Exception $e) {
            return $this->errorResponse($e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
	}



	function updateBuilding(Request $request){
        $rule = [
            'name' => 'required|string',
            'location_id' => 'required',
            'address' => 'required',
            'status' => 'required',
        ];
        $validation = Validator::make($request->all(), $rule);

        if($validation->fails()){
            $return_arr = array('status' => 'error', 'msg' => 'Please fill all required field.');
            return $this->errorResponse('Validation Fails.',Response::HTTP_NOT_ACCEPTABLE);
        }

	    try {

            $building = Building::find($request->get('id'));

            $building->name = $request->get('name');
            $building->location_id = $request->get('location_id');
            $building->address = $request->get('address');
            $building->status = $request->get('status');
            $building = $building->save();

            if($building) {

                $data = [
                    'success' => true,
                    'message' => 'Building record updated',
                    'code' => Response::HTTP_CREATED,
                    'locale'=>'',
                    'data' => [
                        'buildings'=> $building
                    ]
                ];

                return $this->successResponse($data, Response::HTTP_CREATED);
            }

            return $this->errorResponse("Fail to update the data", Response::HTTP_NOT_FOUND);

        } catch(Exception $e) {
	        return $this->errorResponse("Unable to get response", Response::HTTP_BAD_REQUEST);
        }

	}

	function deleteBuilding($id){
	    try {
            $building = Building::find($id);
            $building->delete();

            if($building) {

                $data = [
                    'success' => true,
                    'message' => 'Building record deleted',
                    'code' => Response::HTTP_CREATED,
                    'locale'=>'',
                    'data' => [
                        'buildings'=> $building
                    ]
                ];

                return $this->successResponse($data, Response::HTTP_CREATED);
            }

            return $this->errorResponse("No record found", Response::HTTP_NOT_FOUND);

	    } catch (Exception $e) {
	        return $this->errorResponse('Unable to delete the record', Response::HTTP_BAD_REQUEST);
        }
	}

	private function _storeImage($building, $request) {
        if($request->has('image_url')) {

            $buildingObj = new Image;
            $buildingObj->image_url = $request->image_url;
            $buildingObj->imageable_id = $building->id;
            $buildingObj->imageable_type = 'App\Building';
            $buildingObj->save();
            return $buildingObj;
        }
        return false;
    }

}



