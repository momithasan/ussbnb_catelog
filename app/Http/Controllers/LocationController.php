<?php

namespace App\Http\Controllers;

use App\Building;
use App\Country;
use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use App\Location;
use Exception;
class LocationController extends Controller
{
    //
    use ApiResponser;

    public function __construct()
    {

    }

    public function createLocation(Request $request) {
        $rule = [
            'location_name' => 'required|string',
            'country_id' => 'required'
        ];
        $validation = Validator::make($request->all(), $rule);

        if($validation->fails()){
            $return_arr = array('status' => 'error', 'msg' => 'Please fill all required field.');
            return $this->errorResponse('Validation fails.',$return_arr);
        }

        try {

            $location = Location::create([
                    'location_name' =>  $request->input('location_name'),
                    'country_id' =>  $request->input('country_id'),
                    'status' => 1 /// default value is active=1
                ]);

                $data = [
                    'success' => true,
                    'message' => 'Location created',
                    'code' => Response::HTTP_CREATED,
                    'locale'=>'',
                    'data' => [
                        'locations'=> $location
                    ]
                ];

//                $data['message'] = "Location added successfully";
//                $data['data'] = $location;
                return $this->successResponse($data, Response::HTTP_CREATED);

        } catch (Exception $e) {
            return $this->errorResponse('unable to create the location', Response::HTTP_BAD_REQUEST);
        }
    }

    public function getAllLocations() {
        try {
            $locations = Location::with('country')->get();
            $count = count($locations);

            $data = [
                'success' => true,
                'message' => 'Locations List',
                'count' => $count,
                'code' => Response::HTTP_OK,
                'locale'=>'',
                'data' => [
                    'locations'=> $locations
                ]
            ];

            return $this->successResponse($data, Response::HTTP_CREATED);

        } catch (Exception $e) {
            return $this->errorResponse('unable to create the location', Response::HTTP_BAD_REQUEST);
        }
    }

    public function getLocationById($locationId) {
        try {
            $location = Location::where('id', $locationId)->first();

            $data = [
                'success' => true,
                'message' => 'Single location',
                'code' => Response::HTTP_OK,
                'locale'=>'',
                'data' => [
                    'locations'=> $location
                ]
            ];

            return $this->successResponse($data, Response::HTTP_CREATED);

        } catch (Exception $e) {
            return $this->errorResponse('unable to create the location', Response::HTTP_BAD_REQUEST);
        }
    }

    public function getLocationDetails($locationId) {
        try {
            $location = Location::with('country')->where('id', $locationId)->first();
            $data['message'] = "Location Detail";

            $data['data'] = $location;

            return $this->successResponse($data, Response::HTTP_CREATED);

        } catch (Exception $e) {
            return $this->errorResponse('unable to create the location', Response::HTTP_BAD_REQUEST);
        }
    }

    public function editLocation() {

    }

    public function updateLocation(Request $request, $id) {
        $rule = [
            'location_name' => 'required|string',
            'country_id' => 'required',
            'status' => 'required'
        ];

        $validation = Validator::make($request->all(), $rule);

        if($validation->fails()){
            $return_arr = array('status' => 'error', 'msg' => 'Please fill all required field.');
            return $this->errorResponse('Validation fails. ', Response::HTTP_BAD_REQUEST);
        }

        try {
            $response = Location::where('id', $id)->update([
                    'location_name' =>  $request->input('location_name'),
                    'country_id' =>  $request->input('country_id'),
                    'status' =>  $request->input('status') /// By default active
                ]);

                $data = [
                    'success' => true,
                    'message' => 'Location updated',
                    'code' => Response::HTTP_CREATED,
                    'locale'=>'',
                    'data' => [
                        'locations'=> $response
                    ]
                ];
            return $this->successResponse($data, Response::HTTP_CREATED);

        } catch (Exception $e) {
			return $this->errorResponse('Unable to process the request', Response::HTTP_BAD_REQUEST);
        }
    }

    public function deleteLocation($id) {
        try {
            $location = Location::where('id', $id)->first();

            if($location) {
                $location->delete();
                $data = [
                    'success' => true,
                    'message' => 'Location deleted',
                    'code' => Response::HTTP_OK,
                    'locale'=>'',
                    'data' => [
                        'locations'=> $location
                    ]
                ];

                return $this->successResponse($data, Response::HTTP_CREATED);
            }
            return $this->errorResponse("Location does not exists", Response::HTTP_NOT_FOUND);
        } catch ( Exception $e)
        {
            return $this->errorResponse('Unable to process the request', Response::HTTP_BAD_REQUEST);
        }

    }

    public function SearchLocationByName(Request $request) {
        try {
            $name = $request->get('location_name');

            if(empty($name)) {
                return $this->errorResponse("No record available for this search", Response::HTTP_NOT_FOUND);
            }
            $location = Location::with('country')->where('location_name','like', "%{$name}%")->get();

            if($location) {

                $data = [
                    'success' => true,
                    'message' => 'Location search result',
                    'code' => Response::HTTP_OK,
                    'locale'=>'',
                    'data' => [
                        'location'=> $location
                    ]
                ];

                return $this->successResponse($data, Response::HTTP_FOUND);
            }

            return $this->errorResponse("Location does not exists", Response::HTTP_NOT_FOUND);
        } catch ( Exception $e)
        {
            return $this->errorResponse('Unable to process the request', Response::HTTP_BAD_REQUEST);
        }
    }

}
