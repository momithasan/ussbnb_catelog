<?php

namespace App\Http\Controllers;

use App\Building;
use App\Country;
use App\Location;
use App\Room;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Exception;

class CountryController extends Controller
{
    //
    use ApiResponser;

    public function __construct()
    {

    }

    public function createCountry(Request $request) {

        $rule = [
            'name' => 'required|string|unique:country,name'
        ];
        $validation = Validator::make($request->all(), $rule);

        if($validation->fails()){
            $return_arr = array('status' => 'error', 'msg' => 'Please fill all required field.');
            return $this->errorResponse('Validation fails. ', Response::HTTP_BAD_REQUEST);
        }

        $status = 0;
        if(!empty($request->input('is_active'))) {
            $status = 1;
        }

        try {
            $country = Country::create([
                'name' =>  $request->input('name'),
                'status' =>  $status,
            ]);

            $data = [
                'success' => true,
                'message' => 'Country created',
                'code' => Response::HTTP_CREATED,
                'locale'=>'',
                'data' => [
                    'country'=> $country
                ]
            ];

//                $data['message'] = "Country successfully created";
//                $data['data'] = $response;
            return $this->successResponse($data, Response::HTTP_CREATED);
        } catch (Exception $e) {
            return $this->errorResponse('Unable to process the request', Response::HTTP_BAD_REQUEST);
        }

    }

     public function getCountryList(){
        try
        {
            $countries= Country::all();
            $totalCountries	= count($countries);

            $response = [
                'success' => true,
                'message' => 'List of countries',
                'count' => $totalCountries,
                'code' => Response::HTTP_OK,
                'locale'=>'',
                'data' => [
                    'countries'=> $countries
                ]

            ];
//            $response['message'] = "List of all countries";
//            $response['data']['count'] = $totalCountries;
//            $response['data']['countries'] = $countries;


            return $this->successResponse($response, Response::HTTP_ACCEPTED);
        } catch (Exception $e) {
            return $this->errorResponse('Unable to process the request', Response::HTTP_BAD_REQUEST);
        }
    }

    public function getCountryById($coutryId)
    {
        try
        {
            $country = Country::where('id', $coutryId)->get();

            $response = [
                'success' => true,
                'message' => 'Country by id',
                'code' => Response::HTTP_OK,
                'locale'=>'',
                'data' => [
                    'country'=> $country
                ]

            ];
//            $response['message'] = "Detail of the country";
//            $response['data']['details'] = $country;

            return $this->successResponse($response, Response::HTTP_ACCEPTED);
        } catch (Exception $e) {
            return $this->errorResponse('Unable to process the request', Response::HTTP_BAD_REQUEST);
        }
    }

    public function updateCountry(Request $request, $id)
    {
        $rule = [
            'name' => 'required|string'
        ];

        $validation = Validator::make($request->all(), $rule);

        if($validation->fails()){
            $return_arr = array('status' => 'error', 'msg' => 'Please fill all required field.');
            return $this->errorResponse('Validation fails. ', Response::HTTP_BAD_REQUEST);
        }

        try {
            $response = Country::where('id', $id)->update([
                'name' =>  $request->name,
                'status' =>  $request->is_active /// By default active
            ]);
            if($response) {
                $data = [
                    'success' => true,
                    'message' => 'Country updated',
                    'code' => Response::HTTP_OK,
                    'locale'=>'',
                    'data' => [
                        'country'=> $response
                    ]
                ];

                return $this->successResponse($data, Response::HTTP_OK);
            } else {
                return $this->errorMessage("Error, Unable to update the record", Response::HTTP_NO_CONTENT);
            }
        } catch (Exception $e) {
            return $this->errorResponse('Unable to process the request', Response::HTTP_BAD_REQUEST);
        }
    }

    public function deleteCountry($id) {
        try {
            $country = Country::where('id', $id)->first();
            if($country) {
                $country->delete();

                $data = [
                    'success' => true,
                    'message' => 'Country deleted',
                    'code' => Response::HTTP_OK,
                    'locale'=>'',
                    'data' => [
                        'country'=> $country
                    ]
                ];

//                $data['message'] = "Country successfully deleted";
//                $data['data']['country'] = $country;

                return $this->successResponse($data, Response::HTTP_CREATED);
            }

            return $this->errorResponse("Country does not exists", Response::HTTP_NOT_FOUND);
        } catch ( Exception $e)
        {
            return $this->errorResponse('Unable to process the request', Response::HTTP_BAD_REQUEST);
        }
    }

    public function SearchCountryByName(Request $request) {
        try {
            $name = $request->get('country_name');

            if(empty($name)) {
                return $this->errorResponse("No record available for this search", Response::HTTP_NOT_FOUND);
            }
            $country = Country::where('name','like', "%{$name}%")->get();

            if($country) {

                $data['message'] = "Country search results";
                $data['data']['location'] = $country;

                return $this->successResponse($data, Response::HTTP_FOUND);
            }
            return $this->errorResponse("Country does not exists", Response::HTTP_NOT_FOUND);
        } catch ( Exception $e)
        {
            return $this->errorResponse('Unable to process the request', Response::HTTP_BAD_REQUEST);
        }
    }
}
